package com.dietsodasoftware.stripe;

import static org.junit.Assert.assertTrue;

import com.dietsodasoftware.stripe.model.BalanceTransactionFriend;
import com.dietsodasoftware.stripe.model.ChargeFriend;
import com.dietsodasoftware.stripe.model.CustomerFriend;
import com.dietsodasoftware.stripe.model.InvoiceFriend;
import com.dietsodasoftware.stripe.model.InvoiceItemFriend;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.BalanceTransaction;
import com.stripe.model.Charge;
import com.stripe.model.Customer;
import com.stripe.model.Invoice;
import com.stripe.model.InvoiceItem;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;

import java.security.SecureRandom;
import java.time.ZonedDateTime;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.stream.IntStream;

public class ZebraStripe {
  private String apiKey = "sk_test_x5jbK1g3hJDm0HOGFtLUcVlo";
  private final Random rnd = new SecureRandom(ZonedDateTime.now().toString().getBytes());

  // https://stripe.com/docs/quickstart#collecting-payment-information
  // https://stripe.com/docs/saving-cards

  /**
   * Create the Stripe customer
   * 1. create with the LTN ID in metadata
   * 2. create with the LTN Code in metadata
   * 3. create with the LTN code as the invoice prefix
   * 4. create with an email address: can't charge w/o email address. emails don't need to be unique
   * 5. create with the company name as customer description
   *
   * At time of creation or later,
   * 1. assign the payment method to the customer using the Stripe payment token
   * 2. this can be done at creation or any other time
   */
  @Test
  public void saveCustomerAndChargeLater() throws CardException, APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
    final String cardToken = "tok_1CjuiZCXbz3jINuNdFowplRv";
    final String truckingCode = RandomStringUtils.randomAlphabetic(4).toUpperCase();

    final Customer customer = CustomerFriend.newBuilder(apiKey)
        .addMetadataValue("ltnId", rnd.ints(20000, 50000).iterator().nextInt())
        .addMetadataValue("ltnCode", truckingCode)
        .invoicePrefix(truckingCode)
        .description("Customer Company name")
        .tokenSource(cardToken)
        .create();

    System.out.println("New Customer: " + customer);
  }

  @Test
  public void testScrewup() throws CardException, APIException, AuthenticationException, InvalidRequestException, APIConnectionException {

    final String customerId = "cus_DAFEl9Q01bPM7a";
    final String producerCode = RandomStringUtils.randomAlphabetic(4).toUpperCase();
    final String loadId = producerCode + "-L" + rnd.ints(1000, 10000).iterator().nextInt();

    final Charge charge = ChargeFriend.newBuilder(apiKey)
        .addMetadataValue("loadId", loadId)
        .amountInDollars(50)
        .customerId(customerId)
        .description("Livestock Transport Network Booked Load " + loadId)
        .create();

    System.out.println("Charge: " + charge);
  }

  @Test
  public void updateCustomer() throws CardException, APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
    final String customerId = "cus_DA21VW8Cu4prnG";

    final Customer updated = CustomerFriend.newBuilder(apiKey)
        .invoicePrefix("JBSC")
        .update(customerId);

    System.out.println();
    System.out.println("Updated customer");
    System.out.println(updated);
  }

  @Test
  public void chargeSavedCustomer() throws CardException, APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
    final String customerId = "cus_DA21VW8Cu4prnG";
    final String loadId = "FAC-L3579";

    final Charge charge = ChargeFriend.newBuilder(apiKey)
        .addMetadataValue("loadId", loadId)
        .amountInDollars(50)
        .customerId(customerId)
        .description("Livestock Transport Network Booked Load " + loadId)
        .create();

    System.out.println("Charge: " + charge);
  }

  @Test
  public void testReadInvoice() throws CardException, APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
    final String invId = "in_1CjmbOCXbz3jINuNGk0yvO8K";

    final Invoice invoice = InvoiceFriend.newBuilder(apiKey)
        .load(invId);

    Map<String, Object> ugh = ImmutableMap.of("metadata", ImmutableMap.of("boo", "yah"));
    Invoice updated = invoice.update(ugh, apiKey);

    System.out.println(updated);
  }

  @Test
  public void testLoadChargeDetails() throws CardException, APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
    final String chargeId = "ch_1CjmugCXbz3jINuN96mvYFuM";

    Optional<Charge> charge = ChargeFriend.newBuilder(apiKey)
        .load(chargeId);

    assertTrue(charge.isPresent());

    System.out.println("Charge: ");
    System.out.println(charge.get());
  }

  @Test
  public void testLoadTransactionById() throws CardException, APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
    final String txnId = "txn_1CjmugCXbz3jINuNY6OVuayi";

    final Optional<BalanceTransaction> txn = BalanceTransactionFriend.newFilterBuilder(apiKey)
        .load(txnId);

    assertTrue(txn.isPresent());

    System.out.println();
    System.out.println("Charge customer card transaction");
    System.out.println(txn.get());
  }

  @Test
  public void testLoadTransactionByCharge() throws CardException, APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
    final String chargeId = "ch_1CjmugCXbz3jINuN96mvYFuM";

    final Optional<BalanceTransaction> txn = BalanceTransactionFriend.forCharge(apiKey, chargeId);

    assertTrue(txn.isPresent());

    System.out.println();
    System.out.println("Charged customer card transactions");
    System.out.println(txn.get());
  }

  /**
   * Before this, be sure to create the Stripe customer
   * 1. see {@link #saveCustomerAndChargeLater()} for notes on this.
   *
   * Then,
   * 1. For each load, create an invoice item
   * 2. For each invoice item, add the specific load ID to that item
   * 3. For each invoice item, add the common bid ID, customer code, load count to that item
   * 4. Create invoice, which will gather all invoice items.
   * 5. Invoice metadata must include all items' bid ID and customer code
   * 6. Invoice metadata must include all items' load IDs
   * 7. Pay the invoice immediately.
   * 8. Update the charge metadata to include all items' load IDs
   */
  @Test
  public void livestockTransportNetworkUseCase() throws CardException, APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
    final int loadCount = rnd.nextInt(5) + 1;
    final List<String> loadIds = Lists.newLinkedList();

    final IntStream rndLoadIds = rnd.ints(1000, 10000);
    final Iterator<Integer> loadIdIt = rndLoadIds.iterator();

    final List<String> customerIds = Lists.newArrayList("cus_DA21VW8Cu4prnG", "cus_DAFEl9Q01bPM7a");
    final String customerId = customerIds.get(rnd.nextInt(2));
    final Double amount = 50.0;

    final Customer truckingCustomer = CustomerFriend.newBuilder(apiKey)
        .retrieve(customerId);

    final String truckingCode = truckingCustomer.getMetadata().get("ltnCode");
    final String producerCode = RandomStringUtils.randomAlphabetic(4).toUpperCase();

    final String bidId = truckingCode + "-B" + loadIdIt.next();

    final Map<String, Object> loads = Maps.newLinkedHashMap();
    loads.put("ltnCustomerCode", truckingCode);
    loads.put("loadBidId", bidId);
    loads.put("ltnLoadCount", loadCount);

    for (int i = 0; i < loadCount; i++) {
      final String loadId = producerCode + "-L" + loadIdIt.next();
      final String metadataKey = "ltnLoadId." + (i+1);

      final InvoiceItem item = InvoiceItemFriend.newItemBuilder(apiKey)
          .customerId(customerId)
          .description("Load No. " + loadId)
          .amountInDollars(amount)
          .addMetadataValue("ltnCustomerCode", truckingCode)
          .addMetadataValue("ltnBidId", bidId)
          .addMetadataValue(metadataKey, loadId)
          .create();

      loadIds.add(loadId);
      loads.put(metadataKey, loadId);
    }

    final String ltnLoadInfo = "LTN Marketplace Booked Loads: " + String.join(", ", loadIds);

    System.out.println("Customer " + truckingCode + "(" + truckingCustomer.getEmail() + ") just won a bid " + bidId + " for " + ltnLoadInfo);

    final Invoice invoice = InvoiceFriend.newInvoiceBuilder(apiKey)
        .customerId(customerId)
        .description(ltnLoadInfo)
        .statementDescriptor("LTN Marketplace")
        .addMetadata(loads)
        .dueDate(ZonedDateTime.now().plusSeconds(10))
        .create();

    System.out.println(invoice);

    System.out.println();
    System.out.println("Invoice with metadata");
    System.out.println(invoice);

    final Invoice paid = InvoiceFriend.newBuilder(apiKey).pay(invoice);

    System.out.println();
    System.out.println("Paid invoice");
    System.out.println(paid);

    final Optional<Charge> charge = ChargeFriend.newBuilder(apiKey)
        .load(paid.getCharge());

    assertTrue(charge.isPresent());

    System.out.println();
    System.out.println("Invoice Charge");
    System.out.println(charge.get());

    final Charge updatedCharge = ChargeFriend.newBuilder(apiKey)
        .description(ltnLoadInfo)
        .addMetadata(loads)
        .update(charge.get());

    System.out.println();
    System.out.println("Invoice Charge with LTN load ID");
    System.out.println(updatedCharge);

    System.out.println("Customer " + truckingCode + " (" + truckingCustomer.getEmail() + ") just won a bid " + bidId + " for " + ltnLoadInfo);
  }
}

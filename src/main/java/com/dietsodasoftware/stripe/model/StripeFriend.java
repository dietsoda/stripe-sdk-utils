package com.dietsodasoftware.stripe.model;

import static com.google.common.base.Preconditions.checkNotNull;

import com.google.common.collect.Maps;
import com.stripe.Stripe;
import com.stripe.net.RequestOptions;

import java.util.Collections;
import java.util.Map;

abstract class StripeFriend {
  private final String apiKey;
  private final Map<String, Object> friend = Maps.newHashMap();

  protected StripeFriend(String apiKey) {
    this.apiKey = checkNotNull(apiKey);
  }

  protected void addFriendValue(String name, Object value) {
    friend.put(checkNotNull(name), checkNotNull(value));
  }

  protected <T> T getNestedObject(String name, T defaultIfNull) {
    final T thing = (T) friend.getOrDefault(checkNotNull(name), defaultIfNull);
    friend.put(name, thing);
    return thing;
  }

  protected RequestOptions myDefaultRequestOptions() {
    return myDefaultRequestOptionsBuilder().build();
  }

  protected RequestOptions.RequestOptionsBuilder myDefaultRequestOptionsBuilder() {
    return RequestOptions.builder()
        .setApiKey(apiKey)
        .setConnectTimeout(Stripe.getConnectTimeout())
        .setReadTimeout(Stripe.getReadTimeout())
        ;
  }

  /**
   * return an unmodifiable reference to the underlying map.
   * @return
   */
  protected Map<String, Object> myFriend() {
    return Collections.unmodifiableMap(friend);
  }
}

package com.dietsodasoftware.stripe.model;

import static com.google.common.base.Preconditions.checkNotNull;

import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Charge;
import org.apache.commons.lang3.StringUtils;

import java.util.Optional;

/**
 * Sometimes, the word "payment" is used synonymously with the data available at the Charges API
 *
 * https://stripe.com/docs/charges
 */
public class ChargeFriend extends StripeMetadataFriend<ChargeFriend> {
  private String tokenSource;
  private String customerId;

  private ChargeFriend(String apiKey) {
    super(apiKey);
  }

  public static ChargeFriend newBuilder(String apiKey) {
    return new ChargeFriend(apiKey);
  }

  /**
   * Stripe takes charge amounts in pennies
   */
  public ChargeFriend amountInPennies(int amount) {
    addFriendValue("amount", amount);
    return this;
  }

  /**
   * multiplied by 100, then sets the amount in pennies
   */
  public ChargeFriend amountInDollars(double amount) {
    return amountInPennies(Double.valueOf((amount*100)).intValue());
  }

  /**
   * if not provided, this defaults to "usd"
   */
  public ChargeFriend currency(String value) {
    addFriendValue("currency", checkNotNull(value));
    return this;
  }

  /**
   * Do not store any sensitive information (personally identifiable information, card details,
   * etc.) as metadata or in the charge’s description parameter.
   */
  public ChargeFriend description(String value) {
    addFriendValue("description", checkNotNull(value));
    return this;
  }

  public ChargeFriend cardToken(String value) {
    addFriendValue("source", checkNotNull(value));
    tokenSource = value;
    return this;
  }

  public ChargeFriend customerId(String customerId) {
    addFriendValue("customer", checkNotNull(customerId));
    this.customerId = customerId;
    return this;
  }

  /**
   * https://stripe.com/docs/charges#dynamic-statement-descriptor
   */
  public ChargeFriend statementDescriptor(String value) {
    value = checkNotNull(value).trim();

    if (StringUtils.indexOfAny("<>'\"", value) >= 0 ||
        value.length() > 22) { // magic number: this is Stripe's limit
      throw new IllegalArgumentException("Invalid statement descriptor");
    }

    try {
      value = value.replace(" ", "");
      Double.parseDouble(value);
      throw new IllegalArgumentException("Invalid statement descriptor");
    } catch (NumberFormatException e) {
      // this is good: it isn't simply a number
    }

    addFriendValue("statement_descriptor", checkNotNull(value));
    return this;
  }

  /**
   * by default, charges are captured immediately.  to authorize only and not capture, use this
   * then save the charge ID to capture later.
   */
  public ChargeFriend authorizeOnly() {
    addFriendValue("capture", false);
    return this;
  }

  public Optional<Charge> load(String chargeId) throws CardException, APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
    checkNotNull(chargeId);
    return Optional.ofNullable(Charge.retrieve(chargeId, myDefaultRequestOptions()));
  }

  public Charge create() throws CardException, APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
    if (StringUtils.isBlank(tokenSource) && StringUtils.isBlank(customerId)) {
      throw new IllegalArgumentException("Must provide one of source or customer");
    }

    if (StringUtils.isNotBlank(tokenSource) &&
        StringUtils.isNotBlank(customerId)) {
      throw new IllegalArgumentException("Only one of token source or customer can be specified");
    }

    // default these
    if (myFriend().get("currency") == null) {
      currency("usd");
    }

    if (myFriend().get("capture") == null) {
      addFriendValue("capture", true);
    }

    return Charge.create(myFriend(), myDefaultRequestOptions());
  }

  public Optional<Charge> update(String chargeId) throws CardException, APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
    checkNotNull(chargeId);

    final Optional<Charge> charge = load(chargeId);

    if (charge.isPresent()) {
      final Charge updated = update(charge.get());
      return Optional.of(updated);
    } else {
      return charge;
    }
  }

  public Charge update(Charge charge) throws CardException, APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
    checkNotNull(charge);
    return charge.update(myFriend(), myDefaultRequestOptions());
  }
}

package com.dietsodasoftware.stripe.model;

import static com.google.common.base.Preconditions.checkNotNull;

import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.BalanceTransaction;
import com.stripe.model.BalanceTransactionCollection;

import java.util.Optional;

/**
 * Use this guy to get our Stripe fees for a processed charge.
 *
 * {@link BalanceTransactionCollection#getCount()} and {@link BalanceTransactionCollection#getTotalCount()}
 * are no longer used; it is null always.  Use the pagination APIs.
 */
public class BalanceTransactionFriend extends StripeFriend {
  private BalanceTransactionFriend(String apiKey) {
    super(apiKey);
  }

  public static BalanceTransactionFriend newFilterBuilder(String apiKey) {
    return new BalanceTransactionFriend(apiKey);
  }

  public static Optional<BalanceTransaction> forCharge(String apiKey, String chargeId) throws CardException, APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
    final BalanceTransactionCollection txns = newFilterBuilder(apiKey)
        .chargeId(chargeId)
        .list();

    if (txns == null) {
      // this is bad news ...
      return Optional.empty();
    }

    if (txns.getData().size() > 1) {
      throw new IllegalStateException("This charge has multiple transactions. Expected only one.");
    }

    if (Boolean.TRUE.equals(txns.getHasMore())) {
      throw new IllegalStateException("This charge has multiple transactions. Expected only one.");
    }

    if (txns.getData().isEmpty()) {
      return Optional.empty();
    } else {
      return Optional.of(txns.getData().get(0));
    }
  }

  public Optional<BalanceTransaction> load(String txnId) throws CardException, APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
    checkNotNull(txnId);
    return Optional.ofNullable(BalanceTransaction.retrieve(txnId, myDefaultRequestOptions()));
  }

  public BalanceTransactionFriend chargeId(String chargeId) {
    return filter("source", chargeId);
  }

  public BalanceTransactionFriend filter(String name, Object value) {
    checkNotNull(name);
    checkNotNull(value);

    addFriendValue(name, value);
    return this;
  }

  public BalanceTransactionCollection list() throws CardException, APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
    return BalanceTransaction.list(myFriend(), myDefaultRequestOptions());
  }
}

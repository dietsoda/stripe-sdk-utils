package com.dietsodasoftware.stripe.model;

import static com.google.common.base.Preconditions.checkNotNull;

import com.google.common.base.Preconditions;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Invoice;

import java.time.ZonedDateTime;
import java.util.Map;

/**
 * https://stripe.com/docs/api#invoice_object
 */
public class InvoiceFriend extends StripeMetadataFriend<InvoiceFriend> {
  private InvoiceFriend(String apiKey) {
    super(apiKey);
  }

  public static InvoiceFriend newBuilder(String apiKey) {
    return new InvoiceFriend(apiKey);
  }

  public static ProductInvoiceFriend newInvoiceBuilder(String apiKey) {
    return new ProductInvoiceFriend(apiKey);
  }

  public Invoice pay(String invoiceId) throws CardException, APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
    checkNotNull(invoiceId);
    final Invoice invoice = Invoice.retrieve(invoiceId);
    return pay(invoice);
  }

  public Invoice pay(Invoice invoice) throws CardException, APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
    checkNotNull(invoice);
    return invoice.pay(myDefaultRequestOptions());
  }

  public Invoice load(String id) throws CardException, APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
    checkNotNull(id);
    return Invoice.retrieve(id, myDefaultRequestOptions());
  }

  public Invoice update(Invoice invoice) throws CardException, APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
    checkNotNull(invoice);
    return invoice.update(myFriend(), myDefaultRequestOptions());
  }

  private Invoice create() throws CardException, APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
    return Invoice.create(myFriend(), myDefaultRequestOptions());
  }

  public static class ProductInvoiceFriend {
    private final InvoiceFriend friend;
    private String customerId;

    private ProductInvoiceFriend(String apiKey) {
      this.friend = InvoiceFriend.newBuilder(apiKey);

      billing("charge_automatically");
    }

    public ProductInvoiceFriend customerId(String value) {
      checkNotNull(value);
      friend.addFriendValue("customer", value);
      customerId = value;
      return this;
    }

    public ProductInvoiceFriend manualBilling() {
      billing("send_invoice");
      return this;
    }

    public ProductInvoiceFriend daysUntilDue(int value) {
      manualBilling();
      friend.addFriendValue("days_until_due", value);
      return this;
    }

    public ProductInvoiceFriend description(String value) {
      checkNotNull(value);
      friend.addFriendValue("description", value);
      return this;
    }

    public ProductInvoiceFriend statementDescriptor(String value) {
      checkNotNull(value);
      friend.addFriendValue("statement_descriptor", value);
      return this;
    }

    public ProductInvoiceFriend subscriptionId(String value) {
      checkNotNull(value);
      friend.addFriendValue("subscription", value);
      return this;
    }

    public ProductInvoiceFriend taxPercentage(double tax) {
      friend.addFriendValue("tax_percent", tax);
      return this;
    }

    public ProductInvoiceFriend dueDate(ZonedDateTime due) {
      checkNotNull(due);
      manualBilling();
      final long dueSeconds = due.toEpochSecond();
      friend.addFriendValue("due_date", dueSeconds);
      return this;
    }

    public final ProductInvoiceFriend addMetadataValue(String name, Object value) {
      friend.addMetadataValue(name, value);
      return this;
    }

    public final ProductInvoiceFriend addMetadata(Map<String, Object> value) {
      friend.addMetadata(value);
      return this;
    }

    public Invoice create() throws CardException, APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
      Preconditions.checkArgument(customerId != null);

      return friend.create();
    }

    private ProductInvoiceFriend billing(String value) {
      checkNotNull(value);
      friend.addFriendValue("billing", value);
      return this;
    }
  }
}

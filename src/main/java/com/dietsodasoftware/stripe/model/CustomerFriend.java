package com.dietsodasoftware.stripe.model;

import static com.google.common.base.Preconditions.checkNotNull;

import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Customer;

public class CustomerFriend extends StripeMetadataFriend<CustomerFriend> {
  private CustomerFriend(String apiKey) {
    super(apiKey);
  }

  public static CustomerFriend newBuilder(String apiKey) {
    return new CustomerFriend(apiKey);
  }

  /**
   *  Part of payload for Customer.create
   *  Part of payload for Customer.update
   */
  public CustomerFriend email(String value) {
    addFriendValue("email", checkNotNull(value));
    return this;
  }

  /**
   *  Part of payload for Customer.create
   *  Part of payload for Customer.update
   */
  public CustomerFriend accountBalanceInPennies(int value) {
    addFriendValue("account_balance", value);
    return this;
  }

  /**
   *  Part of payload for Customer.create
   *  Part of payload for Customer.update
   */
  public CustomerFriend businessVatId(String value) {
    addFriendValue("business_vat_id", checkNotNull(value));
    return this;
  }

  /**
   *  Part of payload for Customer.create
   *  Part of payload for Customer.update
   */
  public CustomerFriend coupon(String value) {
    addFriendValue("coupon", checkNotNull(value));
    return this;
  }

  /**
   * usually used in customer update calls
   *  Part of payload for Customer.create
   *  Part of payload for Customer.update
   */
  public CustomerFriend defaultSource(String value) {
    addFriendValue("default_source", checkNotNull(value));
    return this;
  }

  /**
   *  Part of payload for Customer.create
   *  Part of payload for Customer.update
   */
  public CustomerFriend description(String value) {
    addFriendValue("description", checkNotNull(value));
    return this;
  }

  /**
   *  Part of payload for Customer.create
   *  Part of payload for Customer.update
   */
  public CustomerFriend invoicePrefix(String value) {
    addFriendValue("invoice_prefix", checkNotNull(value));
    return this;
  }

  /**
   *  Part of payload for Customer.create
   *  Part of payload for Customer.update
   *
   *  From: https://stripe.com/docs/quickstart#collecting-payment-information
   *
   *  the value provided here should be token.id from the response below.
   *
   *  The response for tucking away a payment method:

      POST https://checkout.stripe.com/api/account/tokens
      content-type: application/x-www-form-urlencoded; charset=UTF-8

      [URL-encoded form]


      [resopnse follows]
      {
         "account" : {
          "email" : "yo@site.net",
          "phone" : "+14802175906",
          "card" : {
            "last4" : "4242",
            "brand" : "Visa",
            "exp_month" : 10,
            "exp_year" : 2018,
            "billing_address" : null
          }
        },
        "token" : {
          "id" : "tok_1CjgCq2eZvKYlo2CHqJ0zPcx",
          "object" : "token",
          "card" : {
            "id" : "card_1CjgCq2eZvKYlo2CZCNPDbeb",
            "object" : "card",
            "address_city" : null,
            "address_country" : null,
            "address_line1" : null,
            "address_line1_check" : null,
            "address_line2" : null,
            "address_state" : null,
            "address_zip" : "12345",
            "address_zip_check" : "pass",
            "brand" : "Visa",
            "country" : "US",
            "cvc_check" : "pass",
            "dynamic_last4" : null,
            "exp_month" : 10,
            "exp_year" : 2018,
            "funding" : "credit",
            "last4" : "4242",
            "metadata" : {},
            "name" : "yo@site.net",
            "tokenization_method" : null
          },
          "client_ip" : "208.115.158.14",
          "created" : 1530592416,
          "email" : "yo@site.net",
          "livemode" : false,
          "type" : "card",
          "used" : false
        }
      }

   *
   * @param value the token.id from the return object above
   */
  public CustomerFriend tokenSource(String value) {
    addFriendValue("source", checkNotNull(value));
    return this;
  }


  /**
   * https://stripe.com/docs/api#create_customer
   *
   * Save the ID of the response to reuse the customer's card for subsequent purchases.
   *
   * Metadata applies here.
   */
  public Customer create() throws CardException, APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
    // what sort of validations do I need to perform?
    // create has no required fields
    return Customer.create(myFriend(), myDefaultRequestOptions());
  }

  public Customer retrieve(String id) throws CardException, APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
    return Customer.retrieve(id, myDefaultRequestOptions());
  }

  /**
   * Metadata applies here.
   */
  public Customer update(String id) throws CardException, APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
    final Customer customer = retrieve(id);

    return customer.update(myFriend(), myDefaultRequestOptions());
  }
}

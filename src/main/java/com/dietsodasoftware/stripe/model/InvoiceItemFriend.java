package com.dietsodasoftware.stripe.model;

import static com.google.common.base.Preconditions.checkNotNull;

import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.InvoiceItem;

/**
 * either amount or quantity.  but not both.
 */
public class InvoiceItemFriend extends StripeMetadataFriend<InvoiceItemFriend> {
  private InvoiceItemFriend(String apiKey) {
    super(apiKey);

    currency("usd");
  }

  public static InvoiceItemFriend newItemBuilder(String apiKey) {
    return new InvoiceItemFriend(apiKey);
  }

  public InvoiceItemFriend customerId(String value) {
    addFriendValue("customer", value);
    return this;
  }

  public InvoiceItemFriend currency(String value) {
    addFriendValue("currency", value);
    return this;
  }

  public InvoiceItemFriend amountInDollars(double value) {
    return amountInPennies(Double.valueOf(value * 100).intValue());
  }

  public InvoiceItemFriend amountInPennies(int value) {
    addFriendValue("amount", value);
    return this;
  }

  public InvoiceItemFriend description(String value) {
    checkNotNull(value);
    addFriendValue("description", value);
    return this;
  }

  public InvoiceItemFriend quantity(int value) {
    addFriendValue("quantity", value);
    return this;
  }

  public InvoiceItem create() throws CardException, APIException, AuthenticationException, InvalidRequestException, APIConnectionException {
    return InvoiceItem.create(myFriend(), myDefaultRequestOptions());
  }
}

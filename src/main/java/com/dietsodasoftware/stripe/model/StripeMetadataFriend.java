package com.dietsodasoftware.stripe.model;

import static com.google.common.base.Preconditions.checkNotNull;

import com.google.common.collect.Maps;

import java.util.Map;

/**
 * appends to the existing metadata in this friend
 *
 *  Part of payload for Customer.create
 *  Part of payload for Customer.update
 *  Part of payload for Charge.create
 */
abstract class StripeMetadataFriend<F extends StripeMetadataFriend> extends StripeFriend {
  private final String metadataFieldName;

  protected StripeMetadataFriend(String apiKey) {
    this(apiKey, "metadata");
  }

  protected StripeMetadataFriend(String apiKey, String metadaFieldName) {
    super(apiKey);
    this.metadataFieldName = checkNotNull(metadaFieldName);
  }

  public final F addMetadata(Map<String, Object> value) {
    final Map<String, Object> md = getNestedObject(metadataFieldName, Maps.newHashMap());
    md.putAll(checkNotNull(value));
    return (F) this;
  }

  public final F addMetadataValue(String name, Object value) {
    final Map<String, Object> md = getNestedObject(metadataFieldName, Maps.newHashMap());
    md.put(checkNotNull(name), checkNotNull(value));
    return (F) this;
  }
}
